package com.jh.custom.config.providers;

import com.jh.custom.config.authentcations.CustomAuth;
import com.jh.custom.exception.CustomAuthException;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

@AllArgsConstructor
public class CustomAuthProvider implements AuthenticationProvider {
    private String key;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        CustomAuth customAuth = (CustomAuth) authentication;
        if (key.equals(customAuth.getKey())){
            customAuth.setAuthenticated(true);
            return customAuth;
        }else throw new CustomAuthException("wrong key "+key);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(CustomAuth.class);
    }
}
