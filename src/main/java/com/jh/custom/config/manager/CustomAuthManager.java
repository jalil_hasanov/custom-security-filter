package com.jh.custom.config.manager;

import com.jh.custom.config.providers.CustomAuthProvider;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

@AllArgsConstructor
public class CustomAuthManager implements AuthenticationManager {
    private String key;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        CustomAuthProvider customAuthProvider = new CustomAuthProvider(key);
        if (customAuthProvider.supports(authentication.getClass())){
            return customAuthProvider.authenticate(authentication);
        }
        return authentication;
    }
}
